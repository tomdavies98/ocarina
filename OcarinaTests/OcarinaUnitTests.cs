﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ocarina.HelperClasses;
using Ocarina.ViewModels;
using System.Collections.ObjectModel;

namespace OcarinaTests
{
    /// <summary>
    /// Summary description for OcarinaUnitTests
    /// </summary>
    [TestClass]
    public class OcarinaUnitTests
    {
        public OcarinaUnitTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void DrawAxis_ReturnsNonNullOnCorrectInput_PassesOnSuccesfullyCreatedLines()
        {
            var lines = DrawAxisLines.DrawSpectrogramAxis(100,100);
            bool result = false;

            if(lines != null && lines[0] != null && lines[1] != null)
            {
                result = true;
            }

            Assert.IsTrue(result);
        }


        [TestMethod]
        public void DrawAxis_ReturnsNonNullListOnIncorrectInput_FailsGivenParametersAreLessThanEqaulToZero()
        {
            var lines = DrawAxisLines.DrawSpectrogramAxis(-1, -1);
            bool result = false;

            if (lines != null && lines[0] != null && lines[1] != null)
            {
                result = true;
            }

            Assert.IsFalse(result);
        }


        [TestMethod]
        public void ProcessFFTValuesIntoList_TurnComplexValueArrayIntoSingleValueListSquareValue_True()
        {
            //var fftTest = new NAudio.DSP.Complex[2048];
            //Complex[] fft_buffer = new Complex[2048];
        }


        [TestMethod]
        public void ProcessFFTValuesIntoList_TurnComplexValueArrayIntoSingleValueListNotSquareValue_True()
        {
            //var fftTest = new NAudio.DSP.Complex[2048];
            //Complex[] fft_buffer = new Complex[2048];
        }

        [TestMethod]
        public void YPositionConverter_ConvertMouseYPositionOnCanvasToFrequencyValue_PassesOnCorrectParameters()
        {
            double result = PositionConverter.YPositionToFrequency(100, 800);
            double expectedResult = 38587.5; //Frequency in hertz
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void YPositionConverter_ReturnMinusOneWhenImageHeightLessThanPosition_FailsOnIncorrectImageHeight()
        {
            double result = PositionConverter.YPositionToFrequency(100, 50);
            double expectedResult = -1; //Expected -1, which is the fail value
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void XPositionConverter_ConvertMouseXPositionOnCanvasToTimeInSeconds_PassesOnCorrectParameters()
        {
            double result = PositionConverter.XPositionToSeconds(100, 800, 30);
            double expectedResult = 3.75; //Expected seconds at mouse point
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void XPositionConverter_ReturnMinusOneWhenImageWidthLessThanPosition_FailsOnIncorrectImageWidth()
        {
            double result = PositionConverter.XPositionToSeconds(100, 50, 30);
            double expectedResult = -1; //Expected -1, which is the fail value
            Assert.AreEqual(expectedResult, result);
        }


        [TestMethod]
        public void XPositionConverter_ReturnMinusOneWhenFileLengthIsInvalid_FailsValueReturnedOnIncorrectFileLength()
        {
            double result = PositionConverter.XPositionToSeconds(100, 50, -1);
            double expectedResult = -1; //Expected -1, which is the fail value
            Assert.AreEqual(expectedResult, result);
        }

        
        [TestMethod]
        public void GetPencilDrawnAverageValues_ReturnsAverageValueOfList_PassesWhenExpectedAverageIsActualAverage()
        {
            MainViewModel mainvm = new MainViewModel();
            ObservableCollection<PencilPointValue> values = 
                new ObservableCollection<PencilPointValue> {
                    new PencilPointValue(6000.0,1),
                    new PencilPointValue(6200.0,6),
                    new PencilPointValue(6100.0,3),
                    new PencilPointValue(6050.0,1)};
            
            double actualResult = mainvm.GetPencilDrawnAverageValues(values);
            double expectedResult = (6000.0 + 6200.0 + 6100.0 + 6050.0) /4; //Expected -1, which is the fail value
            Assert.AreEqual(Convert.ToInt32(expectedResult), Convert.ToInt32(actualResult));
        }


        [TestMethod]
        public void RedrawTimeCursor_OverwritesTimeCursorPositionInLinesListWithNewValue_True()
        {
            Ocarina.ViewModels.MainViewModel mainVm = new Ocarina.ViewModels.MainViewModel();
            System.Collections.ObjectModel.ObservableCollection<Line> Lines = new System.Collections.ObjectModel.ObservableCollection<Line>();
            int x1 = 20;
            int x2 = 20;
            int y1 = 200;
            int y2 = 200;

            Line line1 = new Line(10, 10, 100, 100);
            Line line2 = new Line(x1, x2, y1, y2);

            Lines.Add(line1);
            Lines.Add(line2);
            mainVm.Lines = Lines;

            //Now changing line values to ensure method redraws based on new values
            mainVm.TimeX1 = x1 + 10;
            mainVm.TimeX2 = x2 + 10;
            //Values have moved 10 pixels horizontally 

            System.Collections.ObjectModel.ObservableCollection<Line> lines = mainVm.RedrawTimeCursor();
            bool result = false;

            if(lines[2].X1 == mainVm.TimeX1 && lines[2].X2 == mainVm.TimeX2)
            {
                result = true;
            }

            Assert.IsTrue(result);
        }

















    }
}
