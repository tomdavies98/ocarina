﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ocarina.HelperClasses
{
    public static class PositionConverter
    {
        public static double YPositionToFrequency(double positionY, int ImageHeight)
        {
            //If the requested position is past the height, return fail value
            if(ImageHeight < positionY)
            {
                return -1;
            }

            double multiplyFactor = positionY / ImageHeight;
            double freq = Convert.ToDouble((44100 - (multiplyFactor * 44100)));
            return freq;
        }

        public static double XPositionToSeconds(double positionX, int imageWidth, double fileLengthSeconds)
        {
            if(imageWidth < positionX || fileLengthSeconds <= 0)
            {
                return -1;
            }
            //Converts to seconds
            double multiplyFactor = positionX / imageWidth;
            double time = Convert.ToDouble((multiplyFactor * fileLengthSeconds));

            return time;
        }

        public static double FrequencyToYPostion(double frequency, int imageHeight)
        {
            return (((frequency + 44100) / 44100) * imageHeight);
        }

    }
}
