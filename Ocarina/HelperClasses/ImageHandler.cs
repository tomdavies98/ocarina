﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Ocarina.HelperClasses
{
    public static class ImageHandler
    {

        public static BitmapSource GenerateCroppedImage(BitmapSource source, System.Windows.Point centerPoint, double width, double height, double rotationAngle)
        {

            double sourceDpiX = source.DpiX;
            double sourceDpiY = source.DpiY;

            double wpfUnitsX = 96;
            double wpfUnitsY = 96;

            double centerPointXInScreenUnits = centerPoint.X / sourceDpiX * wpfUnitsX;
            double centerPointYInScreenUnits = centerPoint.Y / sourceDpiY * wpfUnitsY;

            int targetWidth = (int)Math.Round(width, 0);
            int targetHeight = (int)Math.Round(height, 0);

            double targetWidthInScreenUnits = targetWidth / sourceDpiX * wpfUnitsX;
            double targetHeightInScreenUnits = targetHeight / sourceDpiY * wpfUnitsY;

            double sourceWidthInScreenUnits = source.PixelWidth / sourceDpiX * wpfUnitsX;
            double sourceHeightInScreenUnits = source.PixelHeight / sourceDpiY * wpfUnitsY;

            // rotate the master image around the point
            RotateTransform rotateTransform = new RotateTransform();
            rotateTransform.CenterX = centerPointXInScreenUnits;
            rotateTransform.CenterY = centerPointYInScreenUnits;
            rotateTransform.Angle = rotationAngle * -1;

            // move the point up to the top left
            TranslateTransform translateTransform = new TranslateTransform();
            translateTransform.X = -1 * (centerPointXInScreenUnits - targetWidthInScreenUnits / 2);
            translateTransform.Y = -1 * (centerPointYInScreenUnits - targetHeightInScreenUnits / 2);

            TransformGroup transformGroup = new TransformGroup();
            transformGroup.Children.Add(rotateTransform);
            transformGroup.Children.Add(translateTransform);

            // create an image element to do all the manipulation. This is a cheap way of
            // getting around doing the math myself.
            System.Windows.Controls.Image image = new System.Windows.Controls.Image();
            image.Stretch = Stretch.None;
            image.Source = source;
            image.RenderTransform = transformGroup;

            // more ui cruft. This is just for layout
            Canvas container = new Canvas();
            container.Children.Add(image);
            container.Arrange(new Rect(0, 0, source.PixelWidth, source.PixelHeight));

            // render the result to a new bitmap. 
            RenderTargetBitmap target = new RenderTargetBitmap(targetWidth, targetHeight, sourceDpiX, sourceDpiY, PixelFormats.Default);
            target.Render(container);

            return target;
        }



        public static BitmapSource CreateBitmapSourceFromGdiBitmap(Bitmap bitmap)
        {
            if (bitmap == null)
                throw new ArgumentNullException("bitmap");

            var rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);

            var bitmapData = bitmap.LockBits(
                rect,
                ImageLockMode.ReadWrite,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            try
            {
                var size = (rect.Width * rect.Height) * 4;

                return BitmapSource.Create(
                    bitmap.Width,
                    bitmap.Height,
                    bitmap.HorizontalResolution,
                    bitmap.VerticalResolution,
                    PixelFormats.Bgra32,
                    null,
                    bitmapData.Scan0,
                    size,
                    bitmapData.Stride);
            }
            finally
            {
                bitmap.UnlockBits(bitmapData);
            }
        }

    }

}
