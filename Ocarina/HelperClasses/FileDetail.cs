﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ocarina.HelperClasses
{
    public enum StatName
    {
        FileLength,
        MaxFrequency,
        MinFrequency,
        MeanFrequency
    }
    public class FileDetail
    {
        public StatName Stats { get; set; }
        public string Values { get; set; }

        public FileDetail(StatName statName, string statValue)
        {
            Stats = statName;
            Values = statValue;
        }

    }
}
