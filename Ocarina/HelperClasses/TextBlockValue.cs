﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ocarina.HelperClasses
{
    public class TextBlockValue
    {

        public string TextValue { get; set; }
        public int CanvasLeft { get; set; }
        public int CanvasTop { get; set; }


        public TextBlockValue(string text, int canvasLeft, int canvasTop)
        {
            TextValue = text;
            CanvasLeft = canvasLeft;
            CanvasTop = canvasTop;
        }

    }
}
