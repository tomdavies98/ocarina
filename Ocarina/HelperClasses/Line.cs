﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ocarina.HelperClasses
{
    public enum LineMode
    {
        GenericLine,
        DottedLine
    }

    public class Line
    {
        public LineMode LineMode { get; set; }
        public int X1 { get; set; }
        public int Y1 { get; set; }
        public int X2 { get; set; }
        public int Y2 { get; set; }

        private string _colour;
        public string Colour
        {
            get
            {
                return _colour;
            }
            set
            {
                _colour = value;
                NotifyPropertyChanged(Colour);
            }
        }

        public int Width { get; set; }
        public int Height { get; set; }

        public int CanvasLeft { get; set; }
        public int CanvasTop { get; set; }

        public int StrokeThickness { get; set; }
        //Need to account for make square top right to bottom left
        public Line(int x1, int x2, int y1, int y2)
        {
            Colour = "Grey";
            LineMode = LineMode.GenericLine;
            StrokeThickness = 3;
            X1 = x1;
            X2 = x2;
            Y1 = y1;
            Y2 = y2;
        
            Width = Math.Abs(X1 - X2);
            Height = Math.Abs(Y2 - Y1);
            
            if(Height == 0)
            {
                Height = 1;
            }

            if(Width == 0)
            {
                Width = 1;
            }

                

            CanvasLeft = X1;
            CanvasTop = Y1;
        }

        //Constructs dots for dotted line
        public Line(int x1, int x2, int y1, int y2, LineMode lineMode)
        {
            Colour = "Red";
            //Create a small square that can be added to the lines 
            StrokeThickness = 3;
            LineMode = lineMode;

            X1 = x1;
            x2 = x1 + 1;

            Y1 = y1;
            Y2 = y1 + 1;

            Height = 2;
            Width = 2;

            CanvasLeft = X1;
            CanvasTop = Y1;

        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
