﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ocarina.HelperClasses
{
    public class PencilPointValue
    {
        public double Frequency { get; set; }
        public double Time { get; set; }


        public PencilPointValue(double frequency, double time)
        {
            Frequency = frequency;
            Time = time;

        }



    }
}
