﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ocarina.HelperClasses
{
    public static class DrawAxisLines
    {

        private static List<double> _positionValues = new List<double> { 2001,1955,1908,1862,1815,1769,1723,1676,1630,1583,1537,1490,1444,1398,1351,1305,1258,1212,1165,1119,1072,1026};
        private static List<string> _textValues = new List<string> { "1000","2000","3000","4000","5000","6000","7000","8000","9000","10000","11000","12000","13000","14000","15000","16000","17000","18000","19000","20000","21000","22000"};    


        private static List<double> _yPositions = new List<double>();
        private static int _numberOfNotches = 20;
        private static int _notchIncrement { get; set; }
        public static ObservableCollection<TextBlockValue> GetNotchValues(int imageHeight)
        {
            int startingPosition = 1990;

            if(_notchIncrement == null)
            {
                _notchIncrement = 50;
            }

            ObservableCollection<TextBlockValue> textValues = new ObservableCollection<TextBlockValue>();

            for(int i =0;i< _numberOfNotches;++i)
            {
                string textBlockText = PositionConverter.YPositionToFrequency(_yPositions[i], imageHeight).ToString("#.##") + " Hz";
                textValues.Add(new TextBlockValue(textBlockText, 10, startingPosition));
                startingPosition -= _notchIncrement;
            }

            return textValues;
        }




        public static ObservableCollection<Line> DrawSpectrogramAxis(int imageHeight, int imageWidth)
        {

            if(imageHeight <= 0 || imageWidth <=0)
            {
                return null;
            }

            ObservableCollection<Line> linesToDraw = new ObservableCollection<Line>();

            //Add on the line values found in the DrawYAxis method
            linesToDraw = DrawAxis(imageHeight, imageWidth);

            return linesToDraw;
        }


        private static ObservableCollection<Line> DrawAxis(int imageHeight, int imageWidth)
        {
            ObservableCollection<Line> axisValues = new ObservableCollection<Line>();
            int startingPoint = imageHeight;


            int currentYPos = startingPoint;
            int amountOfTicks = 10;
            int heightIncrement = (imageHeight / 2 ) / amountOfTicks;

            Line yAxis = new Line(85, 85, (imageHeight / 2), imageHeight);

            //Draw horizontal X axis
            Line xAxis = new Line(0, imageWidth, currentYPos, currentYPos);

            axisValues.Add(yAxis);
            axisValues.Add(xAxis);

            //Below are reserving a spot that tools can overwrite later
            axisValues.Add(new Line(0, 0, 0, 0));
            axisValues.Add(new Line(0, 0, 0, 0));

            Line zeroNotch = new Line(75, 95, currentYPos, currentYPos);


            return axisValues;
        }



        //New plot methods below

        public static Tuple<ObservableCollection<Line>, ObservableCollection<TextBlockValue>> PlotNotches(int imageHeight)
        {
            //manually plot
            ObservableCollection<Line> axisValues = new ObservableCollection<Line>();
            ObservableCollection<TextBlockValue> textBlocks = new ObservableCollection<TextBlockValue>();

            for(int i = 0; i<_positionValues.Count;++i)
            {
                int positionVal = Convert.ToInt32(_positionValues[i]);
                Line notch = new Line(75, 95, positionVal, positionVal);
                TextBlockValue textBlockVal = new TextBlockValue(_textValues[i], 15, positionVal - 7);

                textBlocks.Add(textBlockVal);
                axisValues.Add(notch);
            }


            return Tuple.Create(axisValues,textBlocks);
        }


    }
}
