﻿using Ocarina.HelperClasses;
using Ocarina.ViewModels;
using Ocarina.Views;
using ScottPlotMicrophoneFFT;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Color = System.Drawing.Color;

namespace Ocarina
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainViewModel ViewModel;

        public MainWindow(MainViewModel mainVm)
        {
            DataContext = ViewModel = mainVm;
           
            InitializeComponent();
        }


        private void Spectrogram_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ViewModel.MouseDown = true;

            //Save mousePos in terms of window
            ViewModel.WindowMouseDownPoint = e.GetPosition(this);

            System.Windows.Point position = new System.Windows.Point(ViewModel.PanelX, ViewModel.PanelY);
            ViewModel.MouseDownPoint = position;

        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject parent)
        where T : DependencyObject
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                var childType = child as T;
                if (childType != null)
                {
                    yield return (T)child;
                }

                foreach (var other in FindVisualChildren<T>(child))
                {
                    yield return other;
                }
            }
        }


        private void Spectrogram_MouseUp(object sender, MouseButtonEventArgs e)
        {
            // Release the mouse capture and stop tracking it.
            ViewModel.MouseDown = false;
            ViewModel.WindowMouseUpPoint = e.GetPosition(this);

            if (ViewModel.SelectedTool == ToolMode.ScreenshotMode)
            {
                ViewModel.MouseUpPoint = e.GetPosition(this);
                ViewModel.Lines[3] = null;
                //ViewModel.Lines[ViewModel.RecordedRectIndex] = null;
                //Save mousePos in terms of window

                //Take a screenshot of the selected area
                ItemsControl itemControl = LogicalTreeHelper.FindLogicalNode(spectrogramScrollViewer, "SpecItem") as ItemsControl;

                var itemTemplate = LogicalTreeHelper.FindLogicalNode(itemControl, "spectrogramTemplate");

                //var t = FindVisualChildren<UIElement>(itemTemplate);

                //UIElement uiElement;
                //Canvas specCanvas;
                //List<UIElement> elementList = new List<UIElement>();

                //for (int i = 0; i < itemControl.Items.Count; i++)
                //{
                //    uiElement = (UIElement)itemControl.ItemContainerGenerator.ContainerFromIndex(i);
                //    elementList.Add(uiElement);
                //    //specCanvas = uiElement as Canvas;
                //    //if(specCanvas != null)
                //    //{
                //    //    //Found Canvas
                //    //    break;
                //    //}
                //}

                //var cnvs = LogicalTreeHelper.FindLogicalNode(spectrogramScrollViewer, "SpectrogramCanvas");

                //var spectrogramCanvas = LogicalTreeHelper.FindLogicalNode(itemControl, "SpectrogramCanvas");
                //ItemsControl spectrogramItem = (ItemsControl)spectrogramScrollViewer.Template.FindName("SpecItem", spectrogramScrollViewer);
                //Canvas spectrogramCanvas = (Canvas)spectrogramItem.Template.FindName("SpectrogramCanvas", spectrogramItem);
                WriteToPng(this);
            }
            else if (ViewModel.SelectedTool == ToolMode.PencilMode)
            {
                var pencilPoints = ViewModel.PencilPointValues;
                double average = ViewModel.GetPencilDrawnAverageValues(pencilPoints);
                
                AverageFrequencyDisplayViewModel averageFreqVm = new AverageFrequencyDisplayViewModel(average.ToString() + " Hz", pencilPoints, ViewModel.FileNameLoaded);
                AverageFrequencyDisplayWindow averageFreqWin = new AverageFrequencyDisplayWindow(averageFreqVm);

                averageFreqWin.Show();
                ViewModel.PencilPointValues = new ObservableCollection<PencilPointValue>();

            }

        }


        private void SpectrogramScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            spectrogramScrollViewer.ScrollToVerticalOffset(2000);
            
        }

        private int scrollWaitCounter = 0;

        private void PlayAudio(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(ViewModel.ChosenFilePath))
            {

                if (!ViewModel._playAudio)
                {
                    ViewModel.ScaleTransformX = 1;
                    ViewModel.ScaleTransformY = 1;

                    ViewModel._playAudio = true;

                    if (ViewModel._fileFinished == true)
                    {
                        Thread moveCursorThread = new Thread(() =>
                        {
                            Thread.CurrentThread.IsBackground = false;
                            MoveTimecursorWithAudio();
                        });

                        moveCursorThread.Start();
                    }
                    ViewModel.AudioController.PlayAudio();
                }
            }
            else
            {
                MessageBox.Show("You must select a file before playback!", "Playback Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

       

        public void MoveTimecursorWithAudio()
        {
            //int incrementValue = Convert.ToInt32(ImageWidth/(FileLength * 1000));
            double speedValue = (((ViewModel.ImageWidth-85) / (ViewModel.FileLength * 1000)) * 100);
            int pixelperTenSeconds = Convert.ToInt32(Math.Ceiling(speedValue));

            ViewModel._fileFinished = false;
            ViewModel.TimeY2 = ViewModel.ImageHeight;
            ViewModel.ScrollbarHorizontalOffset = 0;

            App.Current.Dispatcher.Invoke((Action)delegate
            {
                spectrogramScrollViewer.ScrollToHorizontalOffset(ViewModel.ScrollbarHorizontalOffset);
            });

            while (ViewModel.TimeX1 <= ViewModel.ImageWidth)
            { 
                if (ViewModel._playAudio == true )
                {
                    App.Current.Dispatcher.Invoke((Action)delegate 
                    {
                        speedValue = (((ViewModel.ImageWidth - 100) / (ViewModel.FileLength * 1000)) * 100);

                        //speedValue = (((ViewModel.ImageWidth - 85) / (ViewModel.FileLength * 1000)) * 100);
                        pixelperTenSeconds = Convert.ToInt32(Math.Ceiling(speedValue)) * 1;
                        pixelperTenSeconds = pixelperTenSeconds + Convert.ToInt32(Math.Ceiling(pixelperTenSeconds / 10.0));

                        var copyLines = ViewModel.RedrawTimeCursor();
                        ViewModel.Lines = copyLines;
                        ViewModel.MoveTimeCursorForward(pixelperTenSeconds);
                        //Move the scrollbar forward at this position 

                        if(scrollWaitCounter > 20)
                        {
                            spectrogramScrollViewer.ScrollToHorizontalOffset(ViewModel.ScrollbarHorizontalOffset);
                            ViewModel.ScrollbarHorizontalOffset += pixelperTenSeconds;
                        }
                        else
                        {
                            scrollWaitCounter++;
                        }
                    });
                }
                Thread.Sleep(100);
            }

            //TimeX1 = 85;
            //TimeX2 = 85;
            ViewModel.ScrollbarHorizontalOffset = 0;
            scrollWaitCounter = 0;
            //ViewModel.TimeX1 = 0;
            //ViewModel.TimeX2 = 0;
            ViewModel.TimeX1 = 85;
            ViewModel.TimeX2 = 85;
            ViewModel._fileFinished = true;
        }

        public void PlotGraphs(object sender, RoutedEventArgs e)
        {
            if (ViewModel.FileLength <= 30)
            {
                var frequencyTimeValues = ViewModel.FrequencyTimeValues;

                //Now have a list of frequencies and a list of times that can be passed into new graph method

                if (ViewModel.ChosenFilePath != null)
                {
                    Thread plotter = new Thread(() =>
                    {
                        Thread.CurrentThread.IsBackground = true;
                    //Create another thread for below
                    System.Windows.Forms.Application.EnableVisualStyles();
                        System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
                        System.Windows.Forms.Application.Run(new Form1(frequencyTimeValues, ViewModel.ChosenFilePath, ViewModel.FFTSize));
                    });

                    plotter.Start();
                }
            }
            else
            {
                MessageBox.Show("File length greater than 30 seconds (RAM error)", "Draw Error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        public void WriteToPng(UIElement element)
        {
            var rect = new Rect(element.RenderSize);
            var visual = new DrawingVisual();

            using (var dc = visual.RenderOpen())
            {
                dc.DrawRectangle(new VisualBrush(element), null, rect);
            }

            RenderTargetBitmap bitmap = new RenderTargetBitmap(
                (int)rect.Width, (int)rect.Height, 96, 96, PixelFormats.Default);
            bitmap.Render(visual);

            //var encoder = new PngBitmapEncoder();
            //encoder.Frames.Add(BitmapFrame.Create(bitmap));

            double screenLeft = SystemParameters.VirtualScreenLeft;
            double screenTop = SystemParameters.VirtualScreenTop;
            double screenWidth = SystemParameters.VirtualScreenWidth;
            double screenHeight = SystemParameters.VirtualScreenHeight;

            Bitmap snapedScreenshot;

            MemoryStream stream = new MemoryStream();
            BitmapEncoder encoder = new BmpBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmap));
            encoder.Save(stream);

            Bitmap bitmapImage = new Bitmap(stream);

            //Use midpoint formula to get midpoint

            if (ViewModel.MouseDownPoint != ViewModel.MouseUpPoint)
            {

                int xValue = Convert.ToInt32((ViewModel.WindowMouseDownPoint.X + ViewModel.WindowMouseUpPoint.X) / 2);
                int yValue = Convert.ToInt32((ViewModel.WindowMouseDownPoint.Y + ViewModel.WindowMouseUpPoint.Y) / 2);
                double width = Math.Abs((ViewModel.WindowMouseDownPoint.X - ViewModel.WindowMouseUpPoint.X));
                double height = Math.Abs((ViewModel.WindowMouseDownPoint.Y - ViewModel.WindowMouseUpPoint.Y));

                if (width > 0 && height > 0)
                {

                    System.Windows.Point midPoint = new System.Windows.Point(xValue, yValue);
                    BitmapSource bitmapImageSource = ImageHandler.CreateBitmapSourceFromGdiBitmap(bitmapImage);
                    BitmapSource croppedImageSource = ImageHandler.GenerateCroppedImage(bitmapImageSource, midPoint, width, height, 0);

                    //ScreenshotViewModel screenshotVm = new ScreenshotViewModel(snapedScreenshot);
                    ScreenshotViewModel screenshotVm = new ScreenshotViewModel(croppedImageSource);

                    ScreenshotWindow screenshotWin = new ScreenshotWindow(screenshotVm);
                    screenshotWin.Show();
                }

                //using (var file = File.OpenWrite("output.png"))
                //{
                //    encoder.Save(file);
                //}
            }
        }

        private void SpectrogramCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            TextBlock textBlock = new TextBlock();
            textBlock.Text = "Testing";
            textBlock.Foreground = System.Windows.Media.Brushes.Red;
            Canvas.SetLeft(textBlock, 50);
            Canvas.SetTop(textBlock, ViewModel.ImageHeight-100);
            Canvas canvas = sender as Canvas;
            canvas.Children.Add(textBlock);
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slider = sender as Slider;
            ViewModel.PauseMedia();
            double percentage = slider.Value / slider.Maximum;
            double setValue = ViewModel.ImageWidth * percentage;
            int setIntVal = Convert.ToInt32(setValue);

            //Because there is a buffer on the width
            if (setIntVal >= 85)
            {
                //Set scrollbar new value 
                ViewModel.ScrollbarHorizontalOffset = setIntVal - 300;
                spectrogramScrollViewer.ScrollToHorizontalOffset(ViewModel.ScrollbarHorizontalOffset);

                ViewModel.TimeX1 = setIntVal;
                ViewModel.TimeX2 = setIntVal;
                var copyLines = ViewModel.RedrawTimeCursor();

                ViewModel.Lines = copyLines;

                int mediaPosition = Convert.ToInt32(ViewModel.FileLength * percentage);
                ViewModel.RepositionMediaAudio(mediaPosition);
                
            }
        }

        private void SpectrogramCanvas_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.LeftCtrl)
            {
                //Left control is down
                ViewModel.LeftCtrlDown = true;
            }
        }

        private void SpectrogramCanvas_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftCtrl)
            {
                //Left control is released
                ViewModel.LeftCtrlDown = false;
            }
        }

        private void SpectrogramCanvas_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                //Do up action
                if(ViewModel.LeftCtrlDown)
                {
                    ViewModel.SpectrogramZoomIn();
                }
            }

            else if (e.Delta < 0)
            {
                //Do down action
                if (ViewModel.LeftCtrlDown)
                {
                    ViewModel.SpectrogramZoomOut();
                }
            }
        }
    }
}
