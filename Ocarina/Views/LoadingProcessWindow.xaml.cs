﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Ocarina.Views
{
    /// <summary>
    /// Interaction logic for LoadingProcessWindow.xaml
    /// </summary>
    public partial class LoadingProcessWindow : Window
    {
        public LoadingProcessWindow()
        {
            InitializeComponent();
        }
    }
}
