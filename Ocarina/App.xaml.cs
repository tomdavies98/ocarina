﻿using Ocarina.ViewModels;
using Ocarina.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Ocarina
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {

            MainViewModel mainVm = new MainViewModel();
            MainWindow mainWin = new MainWindow(mainVm);
            mainWin.Show();


            //LoadingProcessWindow load = new LoadingProcessWindow();
            //load.Show();

            base.OnStartup(e);
        }

    }
}
