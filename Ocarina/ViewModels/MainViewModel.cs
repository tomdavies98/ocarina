﻿using Ocarina.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Input;
using NAudio.Dsp; // for FastFourierTransform
using System.Runtime.InteropServices; // for Marshal
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Media;
using NAudio.Wave; // for sound card access
using Microsoft.WindowsAPICodePack.Dialogs;
using Ocarina.HelperClasses;
using System.Collections.ObjectModel;
using System.Threading;
using System.Linq;
using System.Windows;
using Ocarina.Views;
using ScottPlotMicrophoneFFT;
using HelperClasses;
using System.Windows.Data;

namespace Ocarina.ViewModels
{

    public enum ToolMode
    {
        ScreenshotMode,
        PencilMode
    }


    public class MainViewModel : INotifyPropertyChanged
    {
        public System.Windows.Threading.DispatcherTimer TimeCursorTimer = new System.Windows.Threading.DispatcherTimer();

        public double ScrollbarHorizontalOffset = 100;

        public bool MouseDown = false; // Set to 'true' when mouse is held down.
        public bool LeftCtrlDown = false; //set to 'true' on button down
        public System.Windows.Point WindowMouseDownPoint { get; set; }
        public System.Windows.Point WindowMouseUpPoint { get; set; }

        public System.Windows.Point MouseDownPoint { get; set; }
        public System.Windows.Point MouseUpPoint { get; set; }

        public ToolMode SelectedTool = ToolMode.ScreenshotMode;

        private static string ScreenshotToolNotSelected = "/Icons/ScreenshotToolBig.png";
        private static string ScreenshotToolSelected = "/Icons/SelectedScreenshotToolBig.png";

        //Set below to pencil icons
        private static string PencilToolNotSelected = "/Icons/PencilTool.png";
        private static string PencilToolSelected = "/Icons/SelectedPencilTool.png";

        private ObservableCollection<PencilPointValue> _pencilPointValues = new ObservableCollection<PencilPointValue>();
        public ObservableCollection<PencilPointValue> PencilPointValues
        {
            get
            {
                return _pencilPointValues;
            }
            set
            {
                _pencilPointValues = value;
                NotifyPropertyChanged(nameof(PencilPointValues));
            }
        }

        public FrequencyTimeValues FrequencyTimeValues;

        private bool _squareFFTValuesCheckBox = false;
        public bool SquareFFTValuesCheckBox
        {
            get
            {
                return _squareFFTValuesCheckBox;
            }
            set
            {
                _squareFFTValuesCheckBox = value;
                NotifyPropertyChanged(nameof(SquareFFTValuesCheckBox));
            }
        }

        public const string IsVisible = "Visible";
        public const string IsHidden = "Hidden";

        private string _visibility = IsHidden;
        public string Visibility
        {
            get
            {
                return _visibility;
            }
            set
            {
                _visibility = value;
                NotifyPropertyChanged(nameof(Visibility));
            }
        }
       

        private string _pencilToolImagePath = PencilToolNotSelected;
        public string PencilToolImagePath
        {
            get { return _pencilToolImagePath; }
            set
            {
                _pencilToolImagePath = value;
                NotifyPropertyChanged(nameof(PencilToolImagePath));
            }
        }


        private string _screenshotToolImagePath = ScreenshotToolSelected;
        public string ScreenshotToolImagePath
        {
            get { return _screenshotToolImagePath; }
            set
            {
                _screenshotToolImagePath = value;
                NotifyPropertyChanged(nameof(ScreenshotToolImagePath));
            }
        }

        public bool _playAudio = false;
        public bool _fileFinished = true;

        private double _panelX;
        private double _panelY;

        private string _fileNameLoaded;
        public string FileNameLoaded
        {
            get { return _fileNameLoaded; }
            set
            {
                _fileNameLoaded = value;
                NotifyPropertyChanged(nameof(FileNameLoaded));
            }
        }



        public double PanelX
        {
            get { return _panelX; }
            set
            {
                if (value.Equals(_panelX)) return;
                _panelX = value;
                UpdateSelectedTimePostion(_panelX);
                DrawPencilLine();
                NotifyPropertyChanged(nameof(PanelX));
            }
        }

        public double PanelY
        {
            get { return _panelY; }
            set
            {
                if (value.Equals(_panelY)) return;
                _panelY = value;
                UpdateSelectedFrequency(_panelY);
                DrawSelectRectangleOnMouseDown();
                DrawPencilLine();
                NotifyPropertyChanged(nameof(PanelY));
            }
        }

        public int RecordedRectIndex = 0;



        private List<string> _windowTypes;
        public List<string> WindowTypes
        {
            get
            {
                return _windowTypes;
            }
            set
            {
                _windowTypes = value;
                NotifyPropertyChanged(nameof(WindowTypes));
            }
        }


        private string _selectedWindowType;
        public string SelectedWindowType
        {
            get
            {
                return _selectedWindowType;
            }
            set
            {
                _selectedWindowType = value;
                NotifyPropertyChanged(nameof(SelectedWindowType));
            }
        }


        public void DrawPencilLine()
        {
            if (MouseDown)
            {
                if (SelectedTool == ToolMode.PencilMode)
                {
                    int x1 = Convert.ToInt32(PanelX);
                    int x2 = Convert.ToInt32(PanelX);
                    int y1 = Convert.ToInt32(PanelY);
                    int y2 = Convert.ToInt32(PanelY);

                    Line line = new Line(x1, x2, y1, y2, LineMode.DottedLine);
                    PencilPointValue pencilPoint = new PencilPointValue(NumericalFreqPosition, NumericalTimePosition);
                    
                    PencilPointValues.Add(pencilPoint);

                    Lines.Add(line);
                }
            }
        }


        public double GetPencilDrawnAverageValues(ObservableCollection<PencilPointValue> pencilPointValues)
        {
            int size = pencilPointValues.Count;
            if (size == 0)
            {
                return -1;
            }

            double sum = (pencilPointValues.Select(o => o.Frequency)).Sum();
            int average = Convert.ToInt32(Math.Round(sum / size));

            return average;
        }


        public void DrawSelectRectangleOnMouseDown()
        {
            if (MouseDown)
            {

                if (SelectedTool == ToolMode.ScreenshotMode)
                {
                    // When the mouse is held down, reposition the drag selection box.

                    int x1 = Convert.ToInt32(MouseDownPoint.X);
                    int x2 = Convert.ToInt32(PanelX);
                    int y1 = Convert.ToInt32(MouseDownPoint.Y);
                    int y2 = Convert.ToInt32(PanelY);

                    Line line = new Line(x1, x2, y1, y2);


                    //Assign line value to Lines[3] in ViewModel
                    if (Lines.Count < 4)
                    {
                        Lines.Add(line);
                    }
                    else
                    {
                        //Draw select rectangle on mouseDown drag
                        //Lines[Lines.Count - 1] = Lines[3];
                        Lines[3] = line;
                        //Lines.Add(line);
                        //RecordedRectIndex = Lines.Count - 1;


                    }
                }
                

            }
        }

        public void UpdateSelectedTimePostion(double positionX)
        {
            //Currently this includes the area to the left of the axis
            double multiplyFactor = positionX / ImageWidth;
            NumericalTimePosition = ((multiplyFactor * FileLength));
            PositionTime = NumericalTimePosition.ToString("#.##");

        }


        public void UpdateSelectedFrequency(double positionY)
        {
            double multiplyFactor = positionY / ImageHeight;        
            double subFromVal = ((((double)ImageHeight - 100.0) / ((double)ImageHeight - 100)) * 2);// + 1;
            string freq = (44100 - (multiplyFactor * 44100)).ToString("#.##");
            PositionFrequency = freq;
            NumericalFreqPosition = Convert.ToDouble(freq);
        }

      

        private double _fileLength;
        public double FileLength
        {
            get
            {
                return _fileLength;
            }
            set
            {
                _fileLength = value;
                NotifyPropertyChanged(nameof(FileLength));
            }
        }

        private string _positionFrequency;
        public string PositionFrequency
        {
            get
            {
                return _positionFrequency;
            }
            set
            {
                _positionFrequency = value + " Hz";
                NotifyPropertyChanged(nameof(PositionFrequency));
            }
        }

        private double _numericalFreqPosition;
        public double NumericalFreqPosition
        {
            get
            {
                return _numericalFreqPosition;
            }
            set
            {
                _numericalFreqPosition = value;
                NotifyPropertyChanged(nameof(NumericalFreqPosition));

            }
        }


        private double _numericalTimePosition;
        public double NumericalTimePosition
        {
            get
            {
                return _numericalTimePosition;
            }
            set
            {
                _numericalTimePosition = value;
                NotifyPropertyChanged(nameof(NumericalTimePosition));

            }
        }

        private string _positionTime;
        public string PositionTime
        {
            get
            {
                return _positionTime;
            }
            set
            {
                _positionTime = value + " s";
                NotifyPropertyChanged(nameof(PositionTime));
            }
        }

        private ObservableCollection<FileDetail> _fileDetails = new ObservableCollection<FileDetail>();
        public ObservableCollection<FileDetail> FileDetails
        {
            get
            {
                return _fileDetails;
            }
            set
            {
                _fileDetails = value;
                NotifyPropertyChanged(nameof(FileDetails));
            }
        }



        private ObservableCollection<Line> _axisLines;
        public ObservableCollection<Line> AxisLines
        {
            get
            {
                return _axisLines;
            }
            set
            {
                _axisLines = value;
                NotifyPropertyChanged(nameof(AxisLines));

            }
        }

        private ObservableCollection<TextBlockValue> _textValues = new ObservableCollection<TextBlockValue>();
        public ObservableCollection<TextBlockValue> TextValues
        {
            get
            {
                return _textValues;
            }
            set
            {
                _textValues = value;
                NotifyPropertyChanged(nameof(TextValues));
            }
        }




        private ObservableCollection<Line> _lines = new ObservableCollection<Line>();
        public ObservableCollection<Line> Lines
        {
            get
            {
                return _lines;
            }
            set
            {
                _lines = value;
                NotifyPropertyChanged(nameof(Lines));
            }
        }


     





        private double _specScaleFactor;
        public double SpecScaleFactor
        {
            get
            {
                return _specScaleFactor;
            }
            set
            {
                _specScaleFactor = value;
                NotifyPropertyChanged(nameof(SpecScaleFactor));

            }
        }

        private string _scaleTransform;
        public string ScaleTransform
        {
            get
            {
                return _scaleTransform;
            }
            set
            {
                _scaleTransform = value;
                NotifyPropertyChanged(nameof(ScaleTransform));

            }
        }


        private double _scaleTransformX;
        public double ScaleTransformX
        {
            get
            {
                return _scaleTransformX;
            }
            set
            {
                _scaleTransformX = value;
                ScaleTransform = "Zoom: " + Math.Round(ScaleTransformX, 2);
                NotifyPropertyChanged(nameof(ScaleTransformX));
            }
        }


        private double _scaleTransformY;
        public double ScaleTransformY
        {
            get
            {
                return _scaleTransformY;
            }
            set
            {
                _scaleTransformY = value;
                NotifyPropertyChanged(nameof(ScaleTransformY));
            }
        }


        private string _chosenFilePath;
        public string ChosenFilePath
        {
            get
            {
                return _chosenFilePath;
            }
            set
            {
                _chosenFilePath = value;
                NotifyPropertyChanged(nameof(ChosenFilePath));
            } 
        }


        private int _scrollViewerImageWidth = 800;
        public int ScrollViewerImageWidth
        {
            get
            {
                return _scrollViewerImageWidth;
            }
            set
            {
                _scrollViewerImageWidth = value;
                NotifyPropertyChanged(nameof(ScrollViewerImageWidth));
            }
        }


        private int _scrollViewerImageHeight = 800;
        public int ScrollViewerImageHeight
        {
            get
            {
                return _scrollViewerImageHeight;
            }
            set
            {
                _scrollViewerImageHeight = value;
                NotifyPropertyChanged(nameof(ScrollViewerImageHeight));

            }
        }

        private int _imageWidth;
        public int ImageWidth {
            get
            {
                return _imageWidth;
            }
            set
            {
                _imageWidth = value;
                NotifyPropertyChanged(nameof(ImageWidth));
            }
        }


        private int _imageHeight;
        public int ImageHeight {
            get
            {
                return _imageHeight;
            }
            set
            {
                _imageHeight = value;
                NotifyPropertyChanged(nameof(ImageHeight));

            }
        }




        private ImageSource _spectrogramImage;
        public ImageSource SpectrogramImage
        {
            get
            {
                return _spectrogramImage;
            }
            set
            {
                _spectrogramImage = value;
                NotifyPropertyChanged(nameof(SpectrogramImage));
            }
        }

        private static int buffers_captured = 0; // total number of audio buffers filled
        private static int buffers_remaining = 0; // number of buffers which have yet to be analyzed
        private int SampleRate = 44100;

        public AudioControl AudioController { get; set; }

        private static double unanalyzed_max_sec; // maximum amount of unanalyzed audio to maintain in memory
        private static List<short> unanalyzed_values = new List<short>(); // audio data lives here waiting to be analyzed

        private static List<List<double>> spec_data; // columns are time points, rows are frequency points
        private static int spec_width = 600;
        private static int spec_height;
        int pixelsPerBuffer;

        private double _largestFrequency = 0;
        private double _smallestFrequency = 10000000000;

        private static Random rand = new Random();

        // sound card settings
        private int _rate;
        private int _buffer_update_hz;

        // spectrogram and FFT settings
        int fft_size;
        public ICommand EraserPencilLines { get; set; }
        public ICommand ScreenshotModeSelected { get; set; }
        public ICommand PencilModeSelected { get; set; }
        public ICommand LoadFile { get; set; }
        public ICommand PlayAudio { get; set; }
        public ICommand PauseAudio { get; set; }
        public ICommand ZoomIn { get; set; }
        public ICommand ResetZoom { get; set; }
        public ICommand ZoomOut { get; set; }
        public ICommand IncrementSpecScale { get; set; }
        public ICommand DecrementSpecScale { get; set; }


        public ICommand IncrementFFTSize { get; set; }
        public ICommand DecrementFFTSize { get; set; }


        private int _fftSize = 4096;
        public int FFTSize
        {
            get
            {
                return _fftSize;
            }
            set
            {
                _fftSize = value;
                NotifyPropertyChanged(nameof(FFTSize));
            }
        }



        //X values should be the same value for vertical lines
        private int timeX1 = 100;
        public int TimeX1
        {
            get
            {
                return timeX1;
            }
            set
            {
                timeX1 = value;
                NotifyPropertyChanged(nameof(TimeX1));
            }
        }

        //Below is for moving line right
        private int _timeX2 = 100;
        public int TimeX2
        {
            get
            {
                return _timeX2;
            }
            set
            {
                _timeX2 = value;
                NotifyPropertyChanged(nameof(TimeX2));
            }
        }


        //Below is for moving line down
        private int _timeY1 = 1000;
        public int TimeY1
        {
            get
            {
                return _timeY1;
            }
            set
            {
                _timeY1 = value;
                NotifyPropertyChanged(nameof(TimeY1));

            }
        }

        //Below is length of the line downwards
        private int _timeY2 = 100;
        public int TimeY2
        {
            get
            {
                return _timeY2;
            }
            set
            {
                _timeY2 = value;
                NotifyPropertyChanged(nameof(TimeY2));

            }
        }


        private int _timeCursorSpeed = 1;
        public int TimeCursorSpeed
        {
            get
            {
                return _timeCursorSpeed;
            }
            set
            {
                _timeCursorSpeed = value;
                NotifyPropertyChanged(nameof(TimeCursorSpeed));

            }
        }

        //Constructor for the mainviewmodel to setup initial values and bindings
        public MainViewModel()
        {
            

            ScaleTransformX = 1;
            ScaleTransformY = 1;

            SpecScaleFactor = 1;

            WindowTypes = new List<string>() { "HammingWindow", "HannWindow", "BlackmannHarrisWindow", "No Window" };
            SelectedWindowType = WindowTypes[0];

            Lines = new ObservableCollection<Line>();

            EraserPencilLines = new RelayButtonCommand(o => EraseDrawnPencilLines());

            ScreenshotModeSelected = new RelayButtonCommand(o => ScreenshotToolModeSelected());
            PencilModeSelected = new RelayButtonCommand(o => PencilToolModeSelected());

            IncrementSpecScale = new RelayButtonCommand(o => IncrementSpectrogramScale());
            DecrementSpecScale = new RelayButtonCommand(o => DecrementSpectrogramScale());

            IncrementFFTSize = new RelayButtonCommand(o => IncrementFFTSizeSetting());
            DecrementFFTSize = new RelayButtonCommand(o => DecrementFFTSizeSetting());

            LoadFile = new RelayButtonCommand(o => LoadSoundFile());
            PauseAudio = new RelayButtonCommand(o => PauseMedia());
            ZoomIn = new RelayButtonCommand(o => SpectrogramZoomIn());
            ZoomOut = new RelayButtonCommand(o => SpectrogramZoomOut());
            ResetZoom = new RelayButtonCommand(o => SpectrogramResetZoom());
        }


        public void EraseDrawnPencilLines()
        {

            if (Lines != null)
            {
                var copyLines = new ObservableCollection<Line>(Lines);

                for (int i = 0; i < copyLines.Count; ++i)
                {
                    if (copyLines[i] != null && copyLines[i].LineMode == LineMode.DottedLine)
                    {
                        copyLines.Remove(Lines[i]);
                    }
                }

                Lines = copyLines;
            }
        }



        public void ScreenshotToolModeSelected()
        {
            //Selected screenshot tool
            PencilToolImagePath = PencilToolNotSelected;
            ScreenshotToolImagePath = ScreenshotToolSelected;

            //Switch selected tool to screenshot mode
            SelectedTool = ToolMode.ScreenshotMode;
        }


        public void PencilToolModeSelected()
        {
            //Selected pencil tool
            PencilToolImagePath = PencilToolSelected;
            ScreenshotToolImagePath = ScreenshotToolNotSelected;

            //Switch selected tool to pencil mode
            SelectedTool = ToolMode.PencilMode;
        }

        public void IncrementFFTSizeSetting()
        {
            FFTSize += 2;
        }
        public void DecrementFFTSizeSetting()
        {
            FFTSize -= 2;
        }

        public void IncrementSpectrogramScale()
        {
            SpecScaleFactor += 1;
        }
        public void DecrementSpectrogramScale()
        {
            SpecScaleFactor -= 1;
        }

        public void RepositionMediaAudio(int seconds)
        {
            AudioController.RepositionAudioTime(seconds);
        }

        public void MoveTimeCursorForward(int incrementValue)
        {
            TimeX1 += incrementValue;
            TimeX2 += incrementValue;

        }

        public void LoadSoundFile()
        {
            //Reset values incase this file has been loaded after the first
            

            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.InitialDirectory = "C:\\Users";

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                //Ensure that the file extension is .wav
                ChosenFilePath = dialog.FileName;


                if (ChosenFilePath.EndsWith(".wav"))
                {
                    var splitPath = ChosenFilePath.Split('\\');
                    FileNameLoaded = splitPath[splitPath.Length-1];
                    Visibility = IsVisible;

                    Thread drawSpectrogramThread = new Thread(() =>
                    {
                        Thread.CurrentThread.IsBackground = true;
                        LoadSoundFileForPlayBackAndSpectrogram();

                        Visibility = IsHidden;

                    });

                    drawSpectrogramThread.Start();

                }

            }
        }


        public Complex[] PrepareFFTData(int fftSize)
        {
            Complex[] fft_buffer = new Complex[fft_size];

            for (int i = 0; i < fft_size; i++)
            {

                if (SelectedWindowType == "HammingWindow")
                {
                    fft_buffer[i].X = (float)(unanalyzed_values[i] * FastFourierTransform.HammingWindow(i, fft_size));
                }
                else if (SelectedWindowType == "HannWindow")
                {
                    fft_buffer[i].X = (float)(unanalyzed_values[i] * FastFourierTransform.HannWindow(i, fft_size));

                }
                else if (SelectedWindowType == "BlackmannHarrisWindow")
                {
                    fft_buffer[i].X = (float)(unanalyzed_values[i] * FastFourierTransform.BlackmannHarrisWindow(i, fft_size));
                }
                else if (SelectedWindowType == "No Window")
                {
                    fft_buffer[i].X = (float)unanalyzed_values[i]; // no window
                }

                //fft_buffer[i].X = (float)(unanalyzed_values[i] * FastFourierTransform.BlackmannHarrisWindow(i, fft_size));
                fft_buffer[i].Y = 0;
            }

            return fft_buffer;
        }

        public List<double> ProcessFFTValuesIntoList(Complex[] fft_buffer, bool squareFFTValuesCheckBox)
        {
            int loopSize = (spec_data[spec_data.Count - 1].Count);
            List<double> new_data = new List<double>();

            // fill that new data list with fft values (using magnitude here)
            for (int i = 0; i < loopSize; i++)
            {
                double val;
                if (squareFFTValuesCheckBox)
                {
                    val = ((double)fft_buffer[i].X * (double)fft_buffer[i].X) + ((double)fft_buffer[i].Y * (double)fft_buffer[i].Y);

                }
                else
                {
                    val = (double)fft_buffer[i].X + (double)fft_buffer[i].Y;
                }
                val = Math.Abs(val);

                //Calculate frequency here
                if (i <= loopSize / 2 && (fft_buffer[i].X > 0 || fft_buffer[i].Y > 0))
                {
                    double frequency = (i * SampleRate) / FFTSize;

                    if (frequency > _largestFrequency)
                    {
                        _largestFrequency = frequency;
                    }
                    else if (frequency < _smallestFrequency && frequency != 0)
                    {
                        _smallestFrequency = frequency;
                    }
                }

                //val = ((double)fft_buffer[i].X * (double)fft_buffer[i].X) + ((double)fft_buffer[i].Y + (double)fft_buffer[i].Y);
                //if (checkBox1.Checked) val = Math.Log(val);

                new_data.Add(val);
            }

            return new_data;
        }

        public void Analyze_chunk()
        {
            // fill data with FFT info
            short[] data = new short[fft_size];
            data = unanalyzed_values.GetRange(0, fft_size).ToArray();

            // prepare the complex data which will be FFT'd
            Complex[] fft_buffer = PrepareFFTData(fft_size);

            // perform the FFT
            FastFourierTransform.FFT(true, (int)Math.Log(fft_size, 2.0), fft_buffer);

            // insert new data to the right-most (newest) position
            List<double> new_data = ProcessFFTValuesIntoList(fft_buffer, SquareFFTValuesCheckBox);

            for (int i = new_data.Count/2; i<new_data.Count;++i)
            {
                new_data[i] = 0;

            }

            new_data.Reverse();
            spec_data.Insert(spec_data.Count, new_data); // replaces, doesn't append!

            // remove a certain amount of unanalyzed data
            unanalyzed_values.RemoveRange(0, fft_size / pixelsPerBuffer);


        }



        private void Analyze_values()
        {
            //Add labels to display data here
            if (fft_size == 0) return;
            if (unanalyzed_values.Count < fft_size) return;
            while (unanalyzed_values.Count >= fft_size) Analyze_chunk();

        }

        void Update_bitmap_with_data()
        {
            // create a bitmap we will work with
            Bitmap bitmap = new Bitmap(spec_data.Count, spec_data[0].Count, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

            // modify the indexed palette to make it grayscale
            ColorPalette pal = bitmap.Palette;
            for (int i = 0; i < 256; i++)
                pal.Entries[i] = System.Drawing.Color.FromArgb(255, i, i, i);
            bitmap.Palette = pal;

            // prepare to access data via the bitmapdata object
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);

            // create a byte array to reflect each pixel in the image
            byte[] pixels = new byte[bitmapData.Stride * bitmap.Height];

            // fill pixel array with data
            for (int col = 0; col < spec_data.Count; col++)
            {
                // I selected this manually to yield a number that "looked good"
                double scaleFactor;
                scaleFactor = (double)SpecScaleFactor;

                for (int row = 0; row < spec_data[col].Count; row++)
                {
                    int bytePosition = row * bitmapData.Stride + col;
                    double pixelVal = spec_data[col][row] * scaleFactor;
                    pixelVal = Math.Max(0, pixelVal);
                    pixelVal = Math.Min(255, pixelVal);
                    pixels[bytePosition] = (byte)(pixelVal);
                }
            }

            // turn the byte array back into a bitmap
            Marshal.Copy(pixels, 0, bitmapData.Scan0, pixels.Length);
            bitmap.UnlockBits(bitmapData);

            // apply the bitmap to the Canvas
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                SpectrogramImage = BitmapToImageSource(bitmap);
            });


        }

        private BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        private void Update()
        {
            Analyze_values();

            //Add Height buffer to spectrogram
            List<double> empty_data = new List<double>();
            ImageHeight += 100;

            for (int i = 0; i < spec_data.Count; ++i)
            {
                List<double> temp = new List<double>(spec_data[i]);
                temp.AddRange(empty_data);
                spec_data[i] = temp;
            }

            //Display details about the file
            var fileDetails = new ObservableCollection<FileDetail>();
            string fileLen = AudioController.GetSoundLength();
            FileLength = Convert.ToDouble(fileLen);
            fileDetails.Add(new FileDetail(StatName.FileLength, fileLen));
            fileDetails.Add(new FileDetail(StatName.MaxFrequency, _largestFrequency.ToString() + " seconds"));
            fileDetails.Add(new FileDetail(StatName.MinFrequency, "440hz"));
            fileDetails.Add(new FileDetail(StatName.MeanFrequency, "8000hz"));

            FileDetails = fileDetails;
            Update_bitmap_with_data();
        }


        public void LoadSoundFileForPlayBackAndSpectrogram()
        {
            PrepareSoundFileForPlayBack();

            TimeX1 = 100;
            TimeX2 = 100;

            string fileLen = AudioController.GetSoundLength();
            FileLength = Convert.ToDouble(fileLen);

            CreateSpectrogram();

            ////Add Height buffer to spectrogram
            //ImageHeight += 100;
            //for(int i =0;i<spec_data.Count;++i)
            //{
            //    List<double> temp = new List<double>(spec_data[i]);
            //    temp.AddRange(Data_empty);
            //    spec_data[i] = temp;
            //}

            var results = DrawAxisLines.PlotNotches(ImageHeight);

            var _notches = results.Item1;
            var _lines = (DrawAxisLines.DrawSpectrogramAxis(ImageHeight, ImageWidth));

            foreach(var notch in _notches)
            {
                _lines.Add(notch);
            }

            Lines = _lines;
            TextValues = results.Item2;

        }

        List<double> Data_empty = new List<double>();
        public void PrepareSoundFileForPlayBack()
        {
            AudioController = new AudioControl(ChosenFilePath);
        }
        
        public void ResetTimeCursor()
        {
            TimeX1 = 100;
            TimeX2 = 100;
 
            ScrollbarHorizontalOffset = 0;

            if (Lines.Count >= 3)
            {
                //Resetting line value
                Lines[2] = null;
            }
            else
            {
                Lines = null;
            }
            _fileFinished = true;
            //Need to stop thread here 
        }

        public void StopMedia()
        {
            _playAudio = false;
            ResetTimeCursor();
            AudioController.StopAudio();
        }

        public void PauseMedia()
        {
            if (!string.IsNullOrEmpty(ChosenFilePath))
            {
                _playAudio = false;
                AudioController.PauseAudio();

            }
            else
            {
                MessageBox.Show("You must select a file before pausing!", "Playback Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        public void SpectrogramZoomIn()
        {
            ScaleTransformX += 0.1;
            ScaleTransformY += 0.1;
        }


        public void SpectrogramZoomOut()
        {
            ScaleTransformX -= 0.1;
            ScaleTransformY -= 0.1;
        }


        public ObservableCollection<Line> RedrawTimeCursor()
        {
            var copyLines = new ObservableCollection<Line>(Lines);
            Line line = new Line(TimeX1, TimeX2, TimeY1, TimeY2);

            if (copyLines.Count < 3)
            {
                copyLines.Add(line);
            }
            else
            {
                copyLines[2] = line;
            }

            return copyLines;
        }

        public void SpectrogramResetZoom()
        {
            if (!string.IsNullOrEmpty(ChosenFilePath))
            {
                ScaleTransformX = 1;
                ScaleTransformY = 1;
                SpecScaleFactor = 1;


                Visibility = IsVisible;
                Thread drawSpectrogramThread = new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;
                    LoadSoundFileForPlayBackAndSpectrogram();

                    Visibility = IsHidden;

                });

                drawSpectrogramThread.Start();


            }
            else
            {
                MessageBox.Show("You must select a load file before reset!", "Playback Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void CreateSpectrogram()
        {
            // sound card configuration
            _rate = SampleRate;
            _buffer_update_hz = 60;
            pixelsPerBuffer = 10;

            // FFT/spectrogram configuration
            unanalyzed_max_sec = 2.5;
            //fft_size = 4096; // must be a multiple of 2
            fft_size = FFTSize;
            spec_height = fft_size / 2;

            //Resetting the value of data_empty so it doesnt increase every time method is called
            Data_empty = new List<double>();

            // fill spectrogram data with empty values
            spec_data = new List<List<double>>();
            for (int i = 0; i < spec_height; i++) Data_empty.Add(0);
            for (int i = 0; i < 100; i++) spec_data.Add(Data_empty);

            //Read in bytes from Wav file
            byte[] wavContents =  GetWavFile(ChosenFilePath);
            WaveInEventArgs wavInEvent = new WaveInEventArgs(wavContents, wavContents.Length);
            Audio_buffer_captured(wavInEvent);

            Update();

            ImageWidth = spec_data.Count;
            ImageHeight = spec_data[0].Count;

            double blackPoint = spec_data[10][10];

            List<double> frequencyValues = new List<double>();
            List<double> timeValues = new List<double>();
            //Loop through all spec_data values and create frequency and times coordinates from them
            for (int i = 0; i < spec_data.Count; ++i)
            {
                //Can calculate X position in this loop
                double xposPercentage = (double)(i + 1) / (double)ImageWidth;
                //Check col is not 0 before adding
                double averageColFrequency = 0;
                bool satisfied = false;
                int counter = 0;
                for (int j = 0; j < spec_data[0].Count; ++j)
                {
                    double currentValue = spec_data[i][j];
                    //if (currentValue >= 1)
                    if (currentValue > 0.5)
                    {
                        satisfied = true;
                        //Can calculate Y position in this loop
                        double yposPercentage = (double)(j + 1) / (double)ImageHeight;

                        int yPosition = Convert.ToInt32(Math.Round(yposPercentage * ImageHeight));
                        //Now convert the positional data to frequency / timeData and save

                        averageColFrequency += PositionConverter.YPositionToFrequency(yPosition, ImageHeight);
                        counter++;
                    }
                }

                if (satisfied)
                {
                    int xPosition = Convert.ToInt32(Math.Round(xposPercentage * ImageWidth));
                    averageColFrequency = averageColFrequency / counter;
                    frequencyValues.Add(averageColFrequency);
                    timeValues.Add(PositionConverter.XPositionToSeconds(xPosition, ImageWidth, FileLength));

                    //Rest values for next column
                    counter = 0;
                    averageColFrequency = 0;
                    satisfied = false;
                }
                else if(counter == 0)
                {
                    int xPosition = Convert.ToInt32(Math.Round(xposPercentage * ImageWidth));

                    frequencyValues.Add(0);
                    timeValues.Add(PositionConverter.XPositionToSeconds(xPosition, ImageWidth, FileLength));
                }
            }




            FrequencyTimeValues = new FrequencyTimeValues(frequencyValues, timeValues);

            TimeY2 = ImageHeight;
        }


        public void Audio_buffer_captured(WaveInEventArgs args)
        {
            buffers_captured += 1;
            buffers_remaining += 1;

            // interpret as 16 bit audio, so each two bytes become one value
            short[] values = new short[args.Buffer.Length / 2];
            for (int i = 0; i < args.BytesRecorded; i += 2)
            {
                values[i / 2] = (short)((args.Buffer[i + 1] << 8) | args.Buffer[i + 0]);
            }

            // add these values to the growing list, but ensure it doesn't get too big
            unanalyzed_values.AddRange(values);

        }


        public byte[] GetWavFile(string filePath)
        {
            //Should be taking the first 44 bytes off it as theyre header information
            byte[] entireWavBytes = System.IO.File.ReadAllBytes(filePath);
           
            return entireWavBytes;
        }



        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }
}
