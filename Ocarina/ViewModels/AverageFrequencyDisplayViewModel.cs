﻿using Ocarina.Commands;
using Ocarina.HelperClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Ocarina.ViewModels
{
    public class AverageFrequencyDisplayViewModel
    {
        public ICommand CopyToClipboard { get; set; }

        public ICommand SaveValue { get; set; }
        private string _audioFileName { get; set; }
        public AverageFrequencyDisplayViewModel(string averageFreq, ObservableCollection<PencilPointValue> pencilPointValues, string audioFileName)
        {
            AverageFrequency = "Avg Hz: " + averageFreq;
            SaveValue = new RelayButtonCommand(o => SaveValueToTxt());
            CopyToClipboard = new RelayButtonCommand(o => CopyValueToClipboard());
            PencilPointValues = pencilPointValues;
            _audioFileName = audioFileName;
        }

        public void SaveValueToTxt()
        {
            SaveFileDialog save = new SaveFileDialog();
            //save.Filter = ".txt";
            var result = save.ShowDialog();
            save.Filter = "Text File | *.txt";

            if (result == DialogResult.OK)
            {
                string filepath = save.FileName;
                File.WriteAllText(filepath, AverageFrequency + Environment.NewLine);
                string[] strings = filepath.Split('\\');

                string fileName = strings[strings.Length - 1];

                using (StreamWriter outputFile = new StreamWriter(filepath))
                {
                    outputFile.WriteLine("Lines Hertz:Seconds output for => " + _audioFileName);

                    foreach (PencilPointValue pencilPoint in PencilPointValues)
                        outputFile.WriteLine(pencilPoint.Frequency + ":" + pencilPoint.Time);
                }

            }
        }


        private ObservableCollection<PencilPointValue> _pencilPointValues = new ObservableCollection<PencilPointValue>();
        public ObservableCollection<PencilPointValue> PencilPointValues
        {
            get
            {
                return _pencilPointValues;
            }
            set
            {
                _pencilPointValues = value;
                NotifyPropertyChanged(nameof(PencilPointValues));
            }
        }

        public void CopyValueToClipboard()
        {
            System.Windows.Clipboard.SetText(AverageFrequency);

        }


        private string _averageFrequency;
        public string AverageFrequency
        {
            get
            {
                return _averageFrequency;
            }
            set
            {
                _averageFrequency = value;
                NotifyPropertyChanged(nameof(AverageFrequency));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }
}
