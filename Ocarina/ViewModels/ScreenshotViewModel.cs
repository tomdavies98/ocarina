﻿using Ocarina.Commands;
using Ocarina.HelperClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Ocarina.ViewModels
{
    public class ScreenshotViewModel : INotifyPropertyChanged
    {
        public ICommand CopyClipboard { get; set; }
        public ICommand SaveImage { get; set; }

        public ScreenshotViewModel(BitmapSource image)
        {
            Image = image;
            SaveImage = new RelayButtonCommand(o => SaveImageToMachine());
            CopyClipboard = new RelayButtonCommand(o => CopyImageToClipBoard());

        }

        public void CopyImageToClipBoard()
        {
            System.Windows.Clipboard.SetImage(Image);
        }


        public void SaveImageToMachine()
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            var result = save.ShowDialog();

            if (result == DialogResult.OK)
            {
                string filepath = save.FileName;

                using (var fileStream = new FileStream(filepath, FileMode.Create))
                {
                    BitmapEncoder encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(Image));
                    encoder.Save(fileStream);
                }
            }

        }

        private BitmapSource _image;
        public BitmapSource Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
                NotifyPropertyChanged(nameof(Image));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
