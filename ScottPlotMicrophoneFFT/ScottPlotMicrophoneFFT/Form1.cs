﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using NAudio.Wave;
using NAudio.CoreAudioApi;
using HelperClasses;

namespace ScottPlotMicrophoneFFT
{
    public partial class Form1 : Form
    {
        public string AudioFilePath { get; set; }
        private int RATE = 44100; // sample rate of the sound card

        private int BUFFERSIZE { get; set; }

        // prepare class objects
        public BufferedWaveProvider bwp;
        public List<double> FileFrequencies { get; set; }
        public List<double> TimeIncrements { get; set; }
        public FrequencyTimeValues FrequencyTimeValues { get; set; }

        public Form1(FrequencyTimeValues frequencyTimeValues, string filePath, int bufferSize)
        {

            FrequencyTimeValues = frequencyTimeValues;
            BUFFERSIZE = bufferSize;
            AudioFilePath = filePath;
            InitializeComponent();
            SetupGraphLabels();
            StartListeningToMicrophone();
            //this.BackColor = Color.Red;
            timerReplot.Enabled = true;
        }

        public void AudioDataAvailable( WaveInEventArgs e)
        {
            //bwp.AddSamples(e.Buffer, 0, e.BytesRecorded);
            if (e != null)
            {
                bwp.AddSamples(e.Buffer, 0, e.BytesRecorded);
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        public void SetupGraphLabels()
        {
            scottPlotUC1.fig.labelTitle = "Average Frequency Vs Time Graph";
            scottPlotUC1.fig.labelY = "Average Frequency (Hz)";
            scottPlotUC1.fig.labelX = "Time (seconds)";
            scottPlotUC1.BackColor = Color.Red;
            scottPlotUC1.Redraw();
        }


        public byte[] GetWavFile(string filePath)
        {
            byte[] entireWavBytes = System.IO.File.ReadAllBytes(filePath);
            return entireWavBytes;
        }

        public void StartListeningToMicrophone(int audioDeviceNumber = 0)
        {
            byte[] wavContents = GetWavFile(AudioFilePath);
            WaveInEventArgs wavInEvent = new WaveInEventArgs(wavContents, wavContents.Length);
            WaveFormat wavForm = new WaveFormat(44100, 1);

            bwp = new BufferedWaveProvider(wavForm);
            bwp.BufferLength = BUFFERSIZE * 2;
            bwp.DiscardOnBufferOverflow = true;
            AudioDataAvailable(wavInEvent);
        }



        //Commented out below
        private void Timer_Tick(object sender, EventArgs e)
        {
            // turn off the timer, take as long as we need to plot, then turn the timer back on
            timerReplot.Enabled = false;
            PlotLatestData();
            timerReplot.Enabled = true;
        }

        public bool plotOne = true;
        public int numberOfDraws = 0;
        public bool needsAutoScaling = true;
        public void PlotLatestData()
        {
            /*
             * In this method we could pass a list of frequency values vs
             * a list of time values and plot them accordingly to give us the 
             * frequency vs time graph that we require 
             */

            if (plotOne)
            {


                // check the incoming microphone audio
                int frameSize = BUFFERSIZE;
                var audioBytes = new byte[frameSize];
                bwp.Read(audioBytes, 0, frameSize);

                // return if there's nothing new to plot
                if (audioBytes.Length == 0)
                    return;
                if (audioBytes[frameSize - 2] == 0)
                    return;

                // incoming data is 16-bit (2 bytes per audio point)
                int BYTES_PER_POINT = 2;

                // create a (32-bit) int array ready to fill with the 16-bit data
                int graphPointCount = audioBytes.Length / BYTES_PER_POINT;

                // create double arrays to hold the data we will graph
                double[] pcm = new double[graphPointCount];
                double[] fft = new double[graphPointCount];
                double[] fftReal = new double[graphPointCount / 2];

                // populate Xs and Ys with double data
                for (int i = 0; i < graphPointCount; i++)
                {
                    // read the int16 from the two bytes
                    Int16 val = BitConverter.ToInt16(audioBytes, i * 2);

                    // store the value in Ys as a percent (+/- 100% = 200%)
                    pcm[i] = (double)(val) / Math.Pow(2, 16) * 200.0;
                }

                // calculate the full FFT
                fft = FFT(pcm);

                // determine horizontal axis units for graphs
                //Divide by 1000 to get milliseconds
                double pcmPointSpacingMs = RATE / 1000;
                double fftMaxFreq = RATE / 2;
                double fftPointSpacingHz = fftMaxFreq / graphPointCount;

                // just keep the real half (the other half imaginary)
                Array.Copy(fft, fftReal, fftReal.Length);

                // plot the Xs and Ys for both graphs
                scottPlotUC1.Clear();
                scottPlotUC1.PlotXY(FrequencyTimeValues.TimeValues, FrequencyTimeValues.FrequencyValues, Color.Blue);

                // optionally adjust the scale to automatically fit the data
                if (needsAutoScaling)
                {
                    scottPlotUC1.AxisAuto();
                    //scottPlotUC2.AxisAuto();
                    needsAutoScaling = false;
                }

                //scottPlotUC1.PlotSignal(Ys, RATE);

                numberOfDraws += 1;
                lblStatus.Text = $"Analyzed and graphed PCM and FFT data {numberOfDraws} times";

                // this reduces flicker and helps keep the program responsive
                Application.DoEvents();
                plotOne = false;
            }

        }

        private void autoScaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            needsAutoScaling = true;
        }

        private void infoMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string msg = "";
            msg += "left-click-drag to pan\n";
            msg += "right-click-drag to zoom\n";
            msg += "middle-click to auto-axis\n";
            msg += "double-click for graphing stats\n";
            MessageBox.Show(msg);
        }

        private void websiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://github.com/swharden/Csharp-Data-Visualization");
        }

        public double[] FFT(double[] data)
        {
            double[] fft = new double[data.Length];
            System.Numerics.Complex[] fftComplex = new System.Numerics.Complex[data.Length];
            for (int i = 0; i < data.Length; i++)
                fftComplex[i] = new System.Numerics.Complex(data[i], 0.0);
            Accord.Math.FourierTransform.FFT(fftComplex, Accord.Math.FourierTransform.Direction.Forward);
            for (int i = 0; i < data.Length; i++)
                fft[i] = fftComplex[i].Magnitude;
            
            return fft;
        }


    }
}
