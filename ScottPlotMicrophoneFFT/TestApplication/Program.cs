﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScottPlotMicrophoneFFT;

namespace TestApplication
{
    public class Program
    {


        [STAThread]

        static void Main(string[] args)
        {


            Console.WriteLine("Testing Winforms call, input filePath");
            string inputPath = Console.ReadLine();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(inputPath));
        }
    }
}
