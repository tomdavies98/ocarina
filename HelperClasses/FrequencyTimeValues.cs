﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelperClasses
{
    public class FrequencyTimeValues
    {
        public FrequencyTimeValues(List<double> frequencies, List<double> timeValues)
        {
            FrequencyValues = frequencies.ToArray();
            TimeValues = timeValues.ToArray();

        }


        public double[] FrequencyValues { get; set; }
        public double[] TimeValues { get; set; }


    }
}
